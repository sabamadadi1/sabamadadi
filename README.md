<h1 align="center" title="...and I'm happy to see you here :)"> Hello 👋🏻, I'm Saba Madadi (◔◡◔) 👩🏻‍💻 </h1>
<h3 align="center">Computer science student at Shahid Beheshti University among many other things! </h3>

<p align="center"> (●ˇ∀ˇ●) </p>
<p align="center">
  <img width="500" src="GIFs/Welcome.gif" />
</p>
<p align="center"> ƪ(˘⌣˘)ʃ </p>
<p align="left"> <img src="https://komarev.com/ghpvc/?username=sabamadadi&label=Profile%20views&color=0e75b6&style=flat" alt="sabamadadi" /> </p>

- <a href="https://en.sbu.ac.ir/"> <img src="Icons/SBU.png" alt="blender" width="40" height="40"/></a>

<h2 align="left" title="🎆 I'll respond as soon as possible! 🎀"> Contact </h2>

<a href="https://t.me/sabamadadi9"> <img src="Icons/Telegram.webp" alt="blender" width="40" height="40"/></a>

<h2 align="left" title="🦜 You can also find me there! 👀"> Other Profiles </h2>
  
<a href="https://gitlab.com/sabamadadi1"> <img src="Icons/GL.png" alt="blender" width="40" height="40"/></a> <a href="https://www.linkedin.com/in/saba-madadi-8a7374256/"> <img src="Icons/LinkedIn.png" alt="blender" width="40" height="40"/></a> <a href="https://leetcode.com/sabamadadi/"> <img src="Icons/Leetcode.webp" alt="blender" width="40" height="40"/></a> <a href="https://codeforces.com/profile/sabamadadi"> <img src="Icons/Code Forces.webp" alt="blender" width="40" height="40"/></a> <a href="https://stackoverflow.com/users/21433236/saba-madadi"> <img src="Icons/Stack Overflow.png" alt="blender" width="40" height="40"/></a> <a href="https://www.hackerrank.com/sabamadadi9?hr_r=1"> <img src="Icons/HR.png" alt="blender" width="40" height="40"/></a>



<h2 align="left" title=" You can click on each one to see more information! 🔮"> My Toolbox ⚒️</h2>

- Languages
</h3>
<p align="left"> <a href="https://www.python.org/" target="_blank" rel="noreferrer"> <img src="Icons/Python.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.w3schools.com/c/c_intro.php" target="_blank" rel="noreferrer"> <img src="Icons/C (PL).png" alt="blender" width="50" height="50"/> </a> <a href="https://cplusplus.com/" target="_blank" rel="noreferrer"> <img src="Icons/C++.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.w3schools.com/cs/index.php" target="_blank" rel="noreferrer"> <img src="Icons/Csharp.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.w3schools.com/java/" target="_blank" rel="noreferrer"> <img src="Icons/Java.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.r-project.org/" target="_blank" rel="noreferrer"> <img src="Icons/R.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.w3schools.com/html/" target="_blank" rel="noreferrer"> <img src="Icons/Html.png" alt="blender" width="50" height="50"/> </a> <a href="https://developer.mozilla.org/en-US/docs/Web/CSS" target="_blank" rel="noreferrer"> <img src="Icons/CSS.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.mathworks.com/products/matlab.html" target="_blank" rel="noreferrer"> <img src="Icons/Matlab.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.maplesoft.com/" target="_blank" rel="noreferrer"> <img src="Icons/Maple.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.wolfram.com/mathematica/online/?src=google&420&gclid=CjwKCAjwwb6lBhBJEiwAbuVUSpCgt_R9lpPPdKxqgNg18XgfZcRdi3sA_ZGSHJEAjtZWdtHeS2h4PxoC1FAQAvD_BwE" target="_blank" rel="noreferrer"> <img src="Icons/WM.webp" alt="blender" width="50" height="50"/> </a> <a href="https://dart.dev/" target="_blank" rel="noreferrer"> <img src="Icons/Dart.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.w3schools.com/sql/" target="_blank" rel="noreferrer"> <img src="Icons/SQL.png" alt="blender" width="50" height="50"/> </a>
  
- IDEs
</h3>
<p align="left"> <a href="https://jupyter.org/" target="_blank" rel="noreferrer"> <img src="Icons/Jupyter.png" alt="blender" width="50" height="50"/> </a>  <a href="https://www.jetbrains.com/pycharm/" target="_blank" rel="noreferrer"> <img src="Icons/PyCharm.png" alt="blender" width="50" height="50"/> </a> <a href="https://code.visualstudio.com/" target="_blank" rel="noreferrer"> <img src="Icons/VS.png" alt="blender" width="50" height="50"/> </a>  <a href="https://www.jetbrains.com/clion/" target="_blank" rel="noreferrer"> <img src="Icons/CLion.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.bloodshed.net/" target="_blank" rel="noreferrer"> <img src="Icons/DEV.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.jetbrains.com/idea/" target="_blank" rel="noreferrer"> <img src="Icons/IJ.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.sublimetext.com/" target="_blank" rel="noreferrer"> <img src="Icons/Sublime Text.png" alt="blender" width="50" height="50"/> </a> <a href="https://posit.co/download/rstudio-desktop/" target="_blank" rel="noreferrer"> <img src="Icons/Rstudio.png" alt="blender" width="50" height="50"/> </a> <a href="https://github.blog/2022-06-08-sunsetting-atom/" target="_blank" rel="noreferrer"> <img src="Icons/Atom.png" alt="blender" width="50" height="50"/> </a>

- Front
<p align="left"> <a href="https://www.sfml-dev.org/" target="_blank" rel="noreferrer"> <img src="Icons/SFML.png" alt="blender" width="50" height="50"/> </a> <a href="https://gluonhq.com/products/scene-builder/" target="_blank" rel="noreferrer"> <img src="Icons/Scene Builder.webp" alt="blender" width="50" height="50"/> </a> <a href="https://openjfx.io/" target="_blank" rel="noreferrer"> <img src="Icons/JavaFX.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.javatpoint.com/java-swing" target="_blank" rel="noreferrer"> <img src="Icons/Swing.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.qt.io/" target="_blank" rel="noreferrer"> <img src="Icons/QT.png" alt="blender" width="50" height="50"/> </a>

- Database

<p align="left"> <a href="https://www.pgadmin.org/" target="_blank" rel="noreferrer"> <img src="Icons/pgAdmin.png" alt="blender" width="50" height="50"/> </a> <a href="https://www.mongodb.com/" target="_blank" rel="noreferrer"> <img src="Icons/Mongo.png" alt="blender" width="50" height="50"/> </a>

- Others
  
</h3>
<p align="left"> <a href="https://www.android.com/" target="_blank" rel="noreferrer"> <img src="Icons/Android.png" alt="blender" width="50" height="50"/> </a>        <a href="https://www.office.com/" target="_blank" rel="noreferrer"> <img src="Icons/Office.png" alt="blender" width="50" height="50"/> </a> <a href="https://app.diagrams.net/" target="_blank" rel="noreferrer"> <img src="Icons/Diagrams.png" alt="blender" width="50" height="50"/> </a>  <a href="https://flutter.dev/" target="_blank" rel="noreferrer"> <img src="Icons/Flutter.png" alt="blender" width="50" height="50"/> </a>  <a href="https://www.docker.com/" target="_blank" rel="noreferrer"> <img src="Icons/Docker.png" alt="blender" width="50" height="50"/> </a>   <a href="https://www.microsoft.com/en-us/windows?r=1" target="_blank" rel="noreferrer"> <img src="Icons/Windows.png" alt="blender" width="50" height="50"/> </a>  <a href="https://maven.apache.org/" target="_blank" rel="noreferrer"> <img src="Icons/Maven.jpg" alt="blender" width="50" height="50"/> </a>  <a href="https://gradle.org/" target="_blank" rel="noreferrer"> <img src="Icons/Gradle.png" alt="blender" width="50" height="50"/> </a>  <a href="https://kernel.org/" target="_blank" rel="noreferrer"> <img src="Icons/Linux.png" alt="blender" width="50" height="50"/> </a>


<details>
  
  <summary><b>Expand to see my latest project! </b></summary>
    <p>
      
[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=sabamadadi&repo=MelODyHub)](https://github.com/sabamadadi/MelODyHub)
</p>
  
</details>
  
  <p align="center">  ༼ つ ◕_◕ ༽つ </p>
    
<p align="center"><img src="https://github-readme-streak-stats.herokuapp.com/?user=sabamadadi&" alt="sabamadadi" /></p>


<p align="center"> (●'◡'●) </p>

  <p align="center">&nbsp;<img src="https://github-readme-stats.vercel.app/api?username=sabamadadi&show_icons=true&theme=synthwave" alt="sabamadadi"  align="center">&nbsp;<img src="https://github-readme-stats.vercel.app/api/top-langs/?username=sabamadadi&theme=dark&hide_border=false&include_all_commits=true&count_private=true&layout=compact" alt="sabamadadi" /></p>

<p align="center"> (❁´◡`❁) </p>
  
<p align="center">
  <img width="500" src="GIFs/End.gif" />
</p>
  
<p align="center"> (✿◡‿◡) </p>

<p align="center">&nbsp;<img src="https://quotes-github-readme.vercel.app/api?type=horizontal&theme=radical" alt="sabamadadi" /></p>

<p align="center"> ヾ(⌐■_■)ノ♪ </p>

<p align="center">&nbsp;<img src="https://github-contributor-stats.vercel.app/api?username=sabamadadi&limit=5&theme=dark&combine_all_yearly_contributions=true" alt="sabamadadi" /></p>

<p align="center"> ( ﾟдﾟ)つ Bye </p>
